{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# PolyRound\n",
    "[![PyPI version](https://badge.fury.io/py/PolyRound.svg)](https://badge.fury.io/py/PolyRound)\n",
    "\n",
    "Efficient random sampling in convex polytopes relies on a 'rounding' preprocessing step, in which the polytope is rescaled so that the width is as uniform as possible across different dimensions.\n",
    "PolyRound rounds polytopes on the general form:\n",
    "\n",
    "![equation](https://latex.codecogs.com/gif.latex?P&space;:=&space;\\{x&space;\\in&space;\\mathcal{R}^n:&space;A_{eq}x&space;=&space;b_{eq},&space;A_{ineq}x&space;\\leq&space;b_{ineq}\\}) with matrices ![equation](https://latex.codecogs.com/gif.latex?A_{eq}&space;\\in&space;\\mathcal{R}^{m,n}) and ![equation](https://latex.codecogs.com/gif.latex?A_{ineq}\\in&space;\\mathcal{R}^{k,n}) and vectors ![equation](https://latex.codecogs.com/gif.latex?b_{eq}&space;\\in&space;\\mathcal{R}^{m}) and ![equation](https://latex.codecogs.com/gif.latex?b_{ineq}\\in&space;\\mathcal{R}^{k}).\n",
    "\n",
    "This formulation often arises in Systems Biology as the flux space of a metabolic network.\n",
    "\n",
    "As output, PolyRound produces a polytope on the form ![equation](https://latex.codecogs.com/gif.latex?P^{r}&space;:=&space;\\{v&space;\\in&space;\\mathcal{R}^l:&space;A^{r}_{ineq}v&space;\\leq&space;b^{r}_{ineq}\\}) where ![equation](https://latex.codecogs.com/gif.latex?l&space;\\leq&space;n) and the zero vector is a stricly interior point. For transforming points back to the original space, it also provides a matrix ![equation](https://latex.codecogs.com/gif.latex?S&space;\\in&space;\\mathcal{R}^{n,l}) and a vector ![equation](https://latex.codecogs.com/gif.latex?t&space;\\in&space;\\mathcal{R}^{n}), so that ![equation](https://latex.codecogs.com/gif.latex?x&space;=&space;Sv&space;&plus;&space;t).\n",
    "\n",
    "Currently, PolyRound is supported for python 3.7 and 3.8.\n",
    "\n",
    "PolyRound no longer depends on a Gurobi installation and uses optlang (https://github.com/opencobra/optlang) to delegate linear programs to GLPK in case Gurobi is not installed. However, PolyRound is more reliable with Gurobi. Free Gurobi licenses for academic use can be obtained at https://www.gurobi.com/. Once the license is installed, the easiest way to get gurobi to work in python is through Anaconda https://www.anaconda.com/. Installation of gurobi in a conda environment is done with \"conda install -c gurobi gurobi\".\n",
    "\n",
    "An easy example of how to get started is presented in the jupyter notebook cells below.\n"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "They show how to: <br>\n",
    "1) create a polytope object from a file path <br>\n",
    "2) simplify, reduce, and round a polytope in separate steps, togehter with some printed checks <br>\n",
    "3) simplify, reduce and round a polytope in one step <br>\n",
    "4) save the rounded polytope in various formats"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "outputs": [],
   "source": [
    "import os\n",
    "from PolyRound.api import PolyRoundApi\n",
    "from PolyRound.static_classes.lp_utils import ChebyshevFinder\n",
    "from pathlib import Path\n",
    "model_path = os.path.join(\"PolyRound\", \"models\", \"e_coli_core.xml\")\n"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "outputs": [],
   "source": [
    "# Import model and create Polytope object\n",
    "polytope = PolyRoundApi.sbml_to_polytope(model_path)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "code",
   "source": [
    "# Remove redundant constraints and refunction inequality constraints that are de-facto equalities.\n",
    "# Due to these inequalities, the polytope is empty (distance from chebyshev center to boundary is zero)\n",
    "x, dist = ChebyshevFinder.chebyshev_center(polytope)\n",
    "print(dist)\n",
    "simplified_polytope = PolyRoundApi.simplify_polytope(polytope)\n",
    "# Afterwards the distance is non-zero\n",
    "x, dist = ChebyshevFinder.chebyshev_center(simplified_polytope)\n",
    "print(dist)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "execution_count": 17,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[-5.72371304e-14]\n",
      "[0.58198972]\n"
     ]
    }
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2.94777315]\n"
     ]
    }
   ],
   "source": [
    "transformed_polytope = PolyRoundApi.transform_polytope(simplified_polytope)\n",
    "# The distance from the chebyshev center to the boundary changes in the new coordinate system\n",
    "x, dist = ChebyshevFinder.chebyshev_center(transformed_polytope)\n",
    "print(dist)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% Transform the polytope to a definition with only inequality constraints using SVD\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1.00000001]\n",
      "0.039252951585759234\n"
     ]
    }
   ],
   "source": [
    "rounded_polytope = PolyRoundApi.round_polytope(transformed_polytope)\n",
    "# After rounding, the distance from the chebyshev center to the boundary is set to be close to 1\n",
    "x, dist = ChebyshevFinder.chebyshev_center(rounded_polytope)\n",
    "print(dist)\n",
    "\n",
    "# The chebyshev center can be back transformed into an interior point in the simplified space.\n",
    "print(simplified_polytope.border_distance(rounded_polytope.back_transform(x)))\n"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% Round the polytope\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "outputs": [],
   "source": [
    "# simplify, transform and round in one call\n",
    "one_step_rounded_polytope = PolyRoundApi.simplify_transform_and_round(polytope)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "outputs": [],
   "source": [
    "#save to hdf5\n",
    "out_hdf5 = os.path.join(\"PolyRound\", 'output', 'rounded_e_coli_core.hdf5')\n",
    "PolyRoundApi.polytope_to_hdf5(one_step_rounded_polytope, out_hdf5)\n",
    "#save to csv\n",
    "out_csv_dir = os.path.join(\"PolyRound\", 'output', 'e_coli_core')\n",
    "Path(out_csv_dir).mkdir(parents=True, exist_ok=True)\n",
    "PolyRoundApi.polytope_to_csvs(one_step_rounded_polytope, out_csv_dir)\n"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}