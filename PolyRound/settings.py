# ©2020-​2021 ETH Zurich, Axel Theorell

from typing import Dict
from PolyRound.default_settings import default_hp_flags, default_0_width
from cobra.util.solver import solvers

if "gurobi" in solvers:
    pref_backend = "gurobi"
else:
    pref_backend = "glpk"


class PolyRoundSettings:
    def __init__(
        self,
        backend: str = pref_backend,
        hp_flags: Dict = default_hp_flags,
        thresh: float = default_0_width,
        verbose: bool = False,
        sgp: bool = False,
        reduce: bool = True,
        regularize: bool = False,
        check_lps: bool = False,
        simplify_only: bool = False,
        presolve: bool = False,
    ):
        self.backend = backend
        self.hp_flags = hp_flags
        self.thresh = thresh
        self.verbose = verbose
        self.sgp = sgp
        self.reduce = reduce
        self.regularize = regularize
        self.check_lps = check_lps
        self.simplify_only = simplify_only
        self.presolve = presolve
